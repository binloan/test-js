/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  var pluginName = 'accordion';
  var privateVar = null;
  var checkTypeAnimation = function(option) {
    var typeAnimation = [];
    switch (option) {
      case 1:
        typeAnimation.active = 'show';
        typeAnimation.inActive = 'hide';
        break;
      case 2:
        typeAnimation.active = 'slideDown';
        typeAnimation.inActive = 'slideUp';
        break;
    }
    return typeAnimation;
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          title = that.element.find(that.options.title),
          typeAnimationArray = checkTypeAnimation(that.options.animation);

      title.eq(that.options.active).addClass('active').next().show();

      title.off('click.' + pluginName).on('click.' + pluginName, function(){
        var currentTitle = $(this);

        if (currentTitle.siblings('.title').hasClass('active')) {
          this.removeClass('avtive').next()[typeAnimationArray.inActive]();
        }

        currentTitle.addClass('active').next()[typeAnimationArray.active]();
      });
    },
    publicMethod: function(params) {

    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    active : 2,
    title : '> .title',
    content : '> .content',
    animation : 2

  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));
