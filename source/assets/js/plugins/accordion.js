/**
 *  @accodion plugin
 *  @description accordion
 *  @version 1.0
 *  @options

 *    active: number
 *    title
 *    content
 *    animation:
 *      1: show/hide
 *      2: slideUp/slideDown
 *    duration: time effect
 *  @events
 *    event
 *  @methods
 *    init
 *      checkAnimation(): setting type of animation
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  var pluginName = 'accordion';
  var checkAnimation = function(option) {
    var animation = [];
    switch (option) {
      case 1:
        animation.active = 'show';
        animation.inActive = 'hide';
        break;
      case 2:
        animation.active = 'slideDown';
        animation.inActive = 'slideUp';
        break;
    }
    return animation;
  };
  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          titles = that.element.find(that.options.title),
          contents = that.element.find(that.options.content),
          duration = that.options.duration,
          typeAnimationArray = checkAnimation(that.options.animation);

      titles.eq(that.options.active).addClass('active').next()[typeAnimationArray.active](duration);

      titles.off('click.' + pluginName).on('click.' + pluginName, function(){
        var currentTitle = $(this);
        contents[typeAnimationArray.inActive]();
        titles.removeClass('active');
        currentTitle.addClass('active').next()[typeAnimationArray.active](duration);
      });
    },
    publicMethod: function(params) {

    },
    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    active: 0,
    title: '> .title',
    content: '> .content',
    animation: 2,
    duration : 500
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));
