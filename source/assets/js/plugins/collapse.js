/**
 *  @popup-photo
 *  @click popup
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  var pluginName = 'smCollapse';


  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this;
      var title = that.element.find(that.options.title);
      var active = that.options.active;
      var content = that.element.find(that.options.content);

      content.eq(active).slideDown();

      title.on('click', function(){
        var activeItem = that.element.find('.active');
        activeItem.slideUp().removeClass('active');
        $(this).next().slideToggle().addClass('active').not('.active').slideUp();
      });


      // title.off('click').on('click', function(){
      //   var activeItem = that.element.find('.active');
      //   activeItem.slideUp().removeClass('active');
      //   $(this).next().slideToggle().addClass('active');
      // });

    },

    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    title: '> h2',
    content: '.panel',
    active: 0
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));