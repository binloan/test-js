/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
;(function($, window, undefined) {
  var pluginName = 'smSlideShow';

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this;
    that.vars = {};
    that.vars.currentSlider = this.options.active;
    that.vars.viewWidth = this.element.find(this.options.preview).width();// size view
    that.vars.buttonNext = this.element.find(this.options.btnNext);// button next
    that.vars.buttonPrev = this.element.find(this.options.btnPrev);// button prev
    that.vars.itemSlider = this.element.find(this.options.preview + ' li');// .preview li
    that.vars.numberSlider = this.vars.itemSlider.length;// number li
    that.vars.slideList = this.options.preview;//
    // that.vars.itemSlider.width(that.vars.viewWidth);
    // that.vars.slideList.css('width', that.vars.viewWidth * that.vars.numberSlider);

    console.log(that.vars.viewWidth);

    console.log(that.vars.numberSlider);

    // that.vars.buttonPrev.bind('click.'+ pluginName, function(e) {
    //   that.vars.itemSlider.eq(0).before(that.vars.itemSlider.eq(that.vars.numberSlider-1));
    //   that.vars.slideList.css({'margin-left': that.vars.itemSlider * (-1)});
    //   that.resetslide();
    //   e.preventDefault();
    //   that.moveSlide(that.vars.currentSlider);
    // });
    // that.vars.buttonNext.bind('click.'+ pluginName, function(e) {
    //   alert('btn Next');
    //   /* Act on the event */
    //   that.vars.itemSlider.eq(that.vars.itemSlider.eq(that.vars.numberSlider-1)).after(that.vars.itemSlider.eq(0));
    //   // that.vars.currentSlider = that.vars.currentSlider+1;
    //   console.log('currentSlider before move = '+ that.vars.currentSlider);
    //   that.vars.slideList.css({'margin-left': that.vars.currentSlider * (-1)});
    //   that.resetslide();
    //   e.preventDefault();
    //   that.moveSlide(that.vars.currentSlider);
    // });


    },
    publicMethod: function(params) {

    },
    // moveSlide: function(index) {
    //   var that = this;
    //   console.log('index1: ' + index);
    //   index = (index + that.vars.numberSlider) % that.vars.numberSlider;
    //   console.log('index2: ' + index);
    //   that.vars.slideList.animate({'margin-left': that.vars.viewWidth * (-index)});

    //   console.log('margin-left '+ that.vars.viewWidth*(-index));
    //   console.log('currentSlider = ' + index);
    // },
    // resetslide: function(index) {
    //   var that = this;
    //   that.vars.itemSlider = this.element.find(this.options.preview + ' li');
    //   console.log(that.vars.itemSlider );
    // },
    // destroy: function() {
    //   $.removeData(this.element[0], pluginName);
    // },
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.console && console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    btnNext: '.next',
    btnPrev: '.prev',
    preview: '.preview',
    active: 0

  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));
